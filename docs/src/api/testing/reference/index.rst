=========================
Reference Testing Devices
=========================

.. automodule:: ska_tango_base.testing.reference


.. toctree::

  Reference Base Component Manager<reference-base-component-manager>
  Reference Subarray Component Manager<reference-subarray-component-manager>
  Reference Subarray Device<reference-subarray-device>
