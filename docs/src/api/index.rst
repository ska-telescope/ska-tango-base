===
API
===

.. toctree::
  :caption: Subpackages
  :maxdepth: 2

  Base subpackage<base/index>
  Executor subpackage<executor/index>
  Obs subpackage<obs/index>
  Poller subpackage<poller/index>
  Subarray subpackage<subarray/index>
  Testing subpackage<testing/index>


.. toctree::
  :caption: Other devices
  :maxdepth: 1

  Alarm Handler device<alarm-handler-device>
  Capability device<capability-device>
  Logger device<logger-device>
  Controller device<controller-device>
  Tel State device<tel-state-device>


.. toctree::
  :caption: Other modules
  :maxdepth: 1

  Commands<commands>
  Control Model<control-model>
  Faults<faults>
  Long Running Commands API<long-running-commands-api>
  Utils<utils>
