==========================
Subarray Component Manager
==========================

.. automodule:: ska_tango_base.subarray.subarray_component_manager
   :members:
