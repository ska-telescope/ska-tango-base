========
Subarray
========

.. automodule:: ska_tango_base.subarray


.. toctree::

  Subarray Component Manager<component-manager>
  Subarray Device<subarray-device>
