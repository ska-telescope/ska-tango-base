==============
Obs subpackage
==============

.. automodule:: ska_tango_base.obs


.. toctree::

  Obs State Model<obs-state-model>
  Obs Device<obs-device>
