=================
Poller subpackage
=================

.. automodule:: ska_tango_base.poller


.. toctree::

  Poller<poller>
  Polling Component Manager<polling-component-manager>
