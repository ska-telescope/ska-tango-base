===================
Executor subpackage
===================

.. automodule:: ska_tango_base.executor


.. toctree::

  Executor<executor>
  Executor Component Manager<executor-component-manager>
