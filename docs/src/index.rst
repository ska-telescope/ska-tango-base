Welcome to SKA Tango Base documentation!
========================================

.. This package provides base devices and functionality for the Tango
.. devices that provide local monitoring and control (LMC) of SKA telescope
.. components

.. automodule:: ska_tango_base

.. toctree::
  :maxdepth: 1
  :caption: Tutorials

.. toctree::
  :maxdepth: 1
  :caption: How-To Guides

  how-to/getting-started
  how-to/add-an-lrc
  how-to/invoke-an-lrc
  how-to/use-logger
  how-to/use-open-telemetry

.. toctree::
  :maxdepth: 1
  :caption: Concepts

  concepts/component-managers
  concepts/logging-configuration
  concepts/long-running-commands
  concepts/failure-guidelines

.. toctree::
  :maxdepth: 1
  :caption: Reference

  reference/lrc-client-server-protocol
  reference/commanded-state-attributes
  api/index

.. toctree::
  :titlesonly:
  :caption: Releases

  releases/migrating-to-1.0
  releases/migrating-to-1.2
  releases/changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
