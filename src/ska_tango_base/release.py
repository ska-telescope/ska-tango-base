# pylint: disable=invalid-name
# -*- coding: utf-8 -*-
#
# This file is part of the SKA Tango Base project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""Release information for ska_tango_base Python Package."""

name = "ska_tango_base"

version = "1.3.1"

version_info = version.split(".")

description = "A set of generic base devices for SKA Telescope."

author = "SKAO"

# pylint: disable=redefined-builtin
license = "BSD-3-Clause"  # noqa A001

url = "https://www.skatelescope.org/"

# pylint: disable-next=redefined-builtin
copyright = "SKAO"  # noqa A001
