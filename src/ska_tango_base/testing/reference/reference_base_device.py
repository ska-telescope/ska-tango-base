# -*- coding: utf-8 -*-
#
# This file is part of the SKA Tango Base project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""
A reference implementation of an SKA base device for tests.

It inherits from SKABaseDevice.
"""
# pylint: disable=invalid-name
from __future__ import annotations

import importlib.util
from typing import Any, cast

from packaging import version
from ska_control_model import ResultCode
from tango import DevState
from tango import __version__ as tango_version
from tango.server import attribute, command

from ska_tango_base.base import SKABaseDevice
from ska_tango_base.commands import SlowDeviceInitCommand, SubmittedSlowCommand
from ska_tango_base.testing.reference.reference_base_component_manager import (
    FakeBaseComponent,
    ReferenceBaseComponentManager,
)

__all__ = ["SKABaseDevice", "main"]


class ReferenceSkaBaseDevice(SKABaseDevice[ReferenceBaseComponentManager]):
    """Implements a reference SKA base device."""

    __version__ = "1.0.0"

    class InitCommand(SlowDeviceInitCommand):
        """A device init command with better time.

        This command performs the "init_completed" action after it has run.
        """

        def do(self, *args: Any, **kwargs: Any) -> tuple[ResultCode, str]:
            """
            Stateless hook for device initialisation.

            :param args: positional arguments to this do method
            :param kwargs: keyword arguments to this do method

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            self._device.state_at_init = self._device.get_state()
            self._completed()
            return ResultCode.OK, "Init completed"

    @attribute  # type: ignore[misc]
    def stateAtInit(self: ReferenceSkaBaseDevice) -> DevState:
        """Return the state when InitCommand ran."""
        return self.state_at_init

    def create_component_manager(
        self: ReferenceSkaBaseDevice,
    ) -> ReferenceBaseComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a reference subarray component manager.
        """
        return ReferenceBaseComponentManager(
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            _component=FakeBaseComponent(),
        )

    def init_command_objects(self: ReferenceSkaBaseDevice) -> None:
        """Initialise the command handlers for commands supported by this device."""
        super().init_command_objects()
        for command_name, method_name in [
            ("SimulateCommandError", "simulate_command_error"),
            ("SimulateIsCmdAllowedError", "simulate_is_cmd_allowed_error"),
            ("ProgressMsg", "report_progress_message"),
            ("TestTelemetryTracing", "test_telemetry_tracing"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=None,
                ),
            )

    @command(dtype_out="DevVarLongStringArray")  # type: ignore[misc]
    def SimulateCommandError(
        self: ReferenceSkaBaseDevice,
    ) -> tuple[list[ResultCode], list[str]]:
        """
        Simulate a command that raises a CommandError during execution.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("SimulateCommandError")
        result_code, message = handler()
        return ([result_code], [message])

    @command(dtype_out="DevVarLongStringArray")  # type: ignore[misc]
    def SimulateIsCmdAllowedError(
        self: ReferenceSkaBaseDevice,
    ) -> tuple[list[ResultCode], list[str]]:
        """
        Simulate a command with a is_cmd_allowed method that raises an Exception.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("SimulateIsCmdAllowedError")
        result_code, message = handler()
        return ([result_code], [message])

    @command(dtype_out="DevVarLongStringArray")  # type: ignore[misc]
    def ProgressMsg(self: ReferenceSkaBaseDevice) -> tuple[list[ResultCode], list[str]]:
        """
        LRC that reports its progress as a string message.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        handler = self.get_command_object("ProgressMsg")
        result_code, message = handler()
        return ([result_code], [message])

    @command(dtype_out="DevVarLongStringArray")  # type: ignore[misc]
    def TestTelemetryTracing(
        self: ReferenceSkaBaseDevice,
    ) -> tuple[list[ResultCode], list[str]]:
        """
        LRC that calls a LRC on another tango device to test telemetry tracing.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        if (
            version.parse(tango_version) >= version.parse("10.0.0")
            and importlib.util.find_spec("opentelemetry") is not None
        ):
            # pylint: disable=import-outside-toplevel
            from opentelemetry.trace import get_current_span

            print(
                "ReferenceSkaBaseDevice.TestTelemetryTracing trace ID:",
                hex(get_current_span().get_span_context().trace_id),
            )
        handler = self.get_command_object("TestTelemetryTracing")
        result_code, message = handler()
        return ([result_code], [message])

    @command()  # type: ignore[misc]
    def SimulateFault(self: ReferenceSkaBaseDevice) -> None:
        """Simulate a fault state."""
        # pylint: disable=protected-access
        self.component_manager._component.set_fault()

    @command()  # type: ignore[misc]
    def SimulateAlarm(self: ReferenceSkaBaseDevice) -> None:
        """Simulate an alarm state."""
        self.set_state(DevState.ALARM)

    @command(dtype_in=float)  # type: ignore[misc]
    def SetCommandTrackerRemovalTime(
        self: ReferenceSkaBaseDevice, seconds: float
    ) -> None:
        """Set the CommandTracker's removal time.

        Setting the command removal time lower than the default 10s simplifies tests'
        assertions of some LRC attributes.

        :param seconds: of removal timer.
        """
        # pylint: disable=protected-access
        self._command_tracker._removal_time = seconds


# ----------
# Run server
# ----------
def main(*args: str, **kwargs: str) -> int:
    """
    Entry point for module.

    :param args: positional arguments
    :param kwargs: named arguments

    :return: exit code
    """
    return cast(int, ReferenceSkaBaseDevice.run_server(args=args or None, **kwargs))


if __name__ == "__main__":
    main()
